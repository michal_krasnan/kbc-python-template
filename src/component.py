'''
Template Component main class.

'''

import logging
import os
import sys
import csv
import datetime

from pathlib import Path

from kbc.env_handler import KBCEnvHandler

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []

APP_VERSION = '2.0.0'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        # for easier local project setup
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO,
                               data_path=default_data_dir)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validate_config(MANDATORY_PARS)
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)
        # ####### EXAMPLE TO REMOVE
        # intialize instance parameteres

        # ####### EXAMPLE TO REMOVE END

    def run(self):
        '''
        Main execution code
        '''

        print('xxx')
        params = self.cfg_params

        # get first table on the input
        IN_FILE = Path(self.get_input_tables_definitions().pop().full_path)
        # set the output file path dynamically
        OUT_FILE = Path(self.tables_out_path).joinpath('output.csv')

        PRINT_LINES = True #params['print_rows']

        print('Running...')
        with open(IN_FILE, 'r') as input, open(OUT_FILE, 'w+', newline='') as out:
            reader = csv.DictReader(input)
            new_columns = reader.fieldnames
            # append row number col
            new_columns.append('row_number')
            writer = csv.DictWriter(out, fieldnames=new_columns, lineterminator='\n', delimiter=',')
            writer.writeheader()
            for index, l in enumerate(reader):
                # print line
                if PRINT_LINES:
                    print(f'Printing line {index}: {l}')
                # add row number
                l['row_number'] = index
                writer.writerow(l)

        # create a manifest file
        self.configuration.write_table_manifest(file_name=str(OUT_FILE),
                                                destination='out.c-academy-1-michalkrasnan1.output2',
                                                primary_key=['row_number'], incremental=True)

        # print last_update
        print(self.get_state_file().get('last_update'))

        # write last update
        self.write_state_file({"last_update": str(datetime.datetime.now().now())})


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(1)
